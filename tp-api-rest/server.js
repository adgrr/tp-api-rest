// Importation des modules externes
const express = require("express"); // Module JS permettant de créer des endpoints HTTP facilement
const bodyParser = require("body-parser"); // Module JS permettant de tranformer les paramètres en JSON
/**Pour l'authentification */
const auth = require("./auth");

/*
  Paramètrage d'Express. Pas besoin de toucher.
  ------------------------------------------------
*/
// Paramètrage de Express
const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
/**Indiquer Express d'utiliser l'application */
app.use(auth);
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-Type, Authorization, Content-Length, X-Requested-With"
  );
  next();
});
/*
  ------------------------------------------------
*/

/*
  Déclaration des données
*/
const data = {
  items: [
    {
      title: "Item de l'index 0",
      content: "Je suis un contenu position 0",
    },{
    
      title: "Item de l'index 1",
      content: "Je suis un contenu position 1",
    },{
    
      title: "Item de l'index 2",
      content: "Je suis un contenu position 2",
    },{
    
      title: "Item de l'index 3",
      content: "Je suis un contenu position 3",
    },{
    
      title: "Item de l'index 4",
      content: "Je suis un contenu position 4",
    },{
    
      title: "Item de l'index 5",
      content: "Je suis un contenu position 5",
    },{
    
      title: "Item de l'index 6",
      content: "Je suis un contenu position 6",
    },
  ],
};

/*
  Déclaration des endpoints (également appelés *routes*)

  req: Contient toutes les données de la requête reçue: headers, paramètres GET, paramètres POST etc..
  res: Contient des fonctions permettant de répondre à la requête

  Obtenir les paramètres GET: req.query
  Obtenir les paramètres POST: req.body
  Obtenir les "paramètres" dans l'URL: req.params
  Répondre un message text: res.send("Okay, bien reçu")
  Répondre avec un object jSON: res.json({message: "Okay, bien reçu"})
*/
// Lorsqu'on reçoit une requête GET
// Exemple: curl localhost:8080/?index=5
// TODO: Retourner l'item correspondant à l'index envoyé dans la requête


app.get("/", (req, res) => {
  const paramsGet = req.query; // {index: "5"}
  console.log({ paramsGet });
  /**
   * Il y avait une erreur dans cette assigantion. Si on faisait le test ça retournait un [Object object]. 
   * Il ne reconnaissait pas le contenu, car il y avait du texte (String) à l'intérieur.
  */
  const text = data.items[paramsGet.index];
  res.send(text); // On répond à la requête avec un texte
  /**
   * Si la requête est bien écrite elle s'éxécutera SINON un message d'erreur apparait.
   * J'ai voulu faire en sorte que si la requête est correcte mais que l'index n'existe pas: Par exemple 10,
   * Qu'un autre message (distcint du premier) avertisse l'utilisateur, mais j'ai pas trouvé la méthode pour faire ça.
  */
 if(paramsGet.index){
    res.send(text); /**Affiche la réponse en format JSON */
  } else {
    res.send("Requete pas correct...");
  }
});



// Lorsqu'on reçoit une requête POST
// Exemple: curl -X POST -H "Content-Type: application/json" localhost:8080 -d '{"title":"Mon titre"}'
// TODO: Sauvegarder l'item reçu dans le tableau des items
app.post("/", (req, res) => {
  const paramsPost = req.body; // {title: "Mon titre"}
  console.log({ paramsPost });
  /**Pour cette méthode suffit de rajouter cette ligne. Pour ajouter un élément (dans ce cas un item dans le tableau*/
  data.items.push(paramsPost);
  res.json(paramsPost);
});



// Lorsqu'on reçoit une requête DELETE
// Exemple: curl -X DELETE localhost:8080/6
// TODO: Supprimer l'item correspondant à l'index envoyé en paramètre d'URL
app.delete("/:number", (req, res) => {
  const paramsURL = req.params; //  {number: "6"}
  console.log({ paramsURL });
  /** 1ère façon non fonctionnelle comme dans javascript.js
   * Elle marche pas du tout. 
   * Mais j'ai compris le fonctionement général. 
   * Ce qu'on va faire c'est un filtrage sur le tableau d'Objet d'items en retirant l'index correspandant à la valeur entrée en paramètre
   * Et on va re-assigner ce tableau mais sans l'item en question.
   * cela évite d'avoir un tableau avec un trou et donc d'en avoir un toujours plein.
   * Le parseInt permet  d'être sûr que c'est un int et que la comparaison se fasse correctement.
   * */
  //data.items = data.items.filter((item, index) => index !== paramsURL);
  /**
   * 2ème façcon non fonctionnelle comme dans javascript.js
   * ça ne fait rien, car il faut que j'accède à la value mais j'ai pas trouvé comment extraire la valeur à {number: 6} par exemple
   * Ici c'est mis en statique du coup ça sera toujours la vlauer à la position 0 qui sera vidé.
   * 
   */
  delete data.items[0];
  res.json(paramsURL);
});

// Lorsqu'on reçoit une requête PUT
// Exemple: curl -X PUT -H "Content-Type: application/json" localhost:8080/?index=2 -d '{"newTitle":"Mon nouveau titre"}'
// TODO: Modifier l'item correspondant à l'index reçu en paramètre GET avec les données reçues en paramètre POST
app.put("/", (req, res) => {
  /** 
   * On récupère l'index (position dans le tableau) ainsi que le contenu qu'on veut modifier
   * Grâce à cette position suffit de faire une assigantion avec le nouveau contenu.
  */
  const paramsGet = req.query.id; // {index: 2}
  const paramsPost = req.query.title; // {newTitle: "Mon nouveau titre"}
  console.log({ paramsPost });
  data[paramsGet].title = paramsPost;
  res.json(paramsGet);
});

/*
  Lancement du serveur sur le port 8080
*/
app.listen(8080, () => console.log(`Listen on port 8080`));
